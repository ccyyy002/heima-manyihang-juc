package cn.itcast.test;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.Test7")
public class Test7 {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread("t1") {
            @Override
            public void run() {
                log.debug("enter sleep...");
                try {
                    Thread.sleep(2000);
                    log.debug("打断后的操作不会执行了");
                } catch (InterruptedException e) {
                    log.debug("wake up...");
                    e.printStackTrace();
                }
            }
        };
        t1.start();
        System.out.println(t1.getState());

        Thread.sleep(1000);
        log.debug("interrupt...");
        t1.interrupt();
        System.out.println(t1.getState());
    }
}
