package cn.itcast.n2;

import cn.itcast.Constants;
import cn.itcast.n2.util.FileReader;
import cn.itcast.utils.MyThreadInfo;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;


@Slf4j(topic = "c.Async")
public class Async {
    @SneakyThrows
    public static void main(String[] args) {
        new Thread(() -> FileReader.read(Constants.MP4_FULL_PATH)).start();
        MyThreadInfo.getThreadInfo();
        log.debug("do other things ...");
        Thread.sleep(50000);
    }
}
