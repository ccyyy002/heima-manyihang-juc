package com.lin;

import java.util.*;
import java.util.concurrent.atomic.AtomicMarkableReference;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @Author zimingl
 * @Date 2023/4/30 21:48
 * @Description: TODO
 */
public class AtomicTest {
    public static void main(String[] args) {
        atomicReferenceTest();
        atomicMarkableReferenceTest();
        atomicStampedReferenceTest();

        Deque<Integer> stack = new LinkedList<>();
        Deque<Integer> queue = new LinkedList<>();

        Deque<Integer> stack2 = new ArrayDeque<>();
        Deque<Integer> queue2 = new ArrayDeque<>();
    }

    private static void atomicReferenceTest() {
        AtomicReference<String> atomicReference = new AtomicReference<>("hello");
        System.out.println("before update: " + atomicReference.get());
        atomicReference.compareAndSet("hello", "world");
        System.out.println("after update: " + atomicReference.get());
    }
    private static void atomicMarkableReferenceTest() {
        AtomicMarkableReference<String> atomicMarkableReference = new AtomicMarkableReference<>("hello", false);
        System.out.println("before update: " + atomicMarkableReference.getReference() + ", mark: " + atomicMarkableReference.isMarked());
        atomicMarkableReference.compareAndSet("hello", "world", false, true);
        System.out.println("after update: " + atomicMarkableReference.getReference() + ", mark: " + atomicMarkableReference.isMarked());
    }
    private static void atomicStampedReferenceTest() {
        AtomicStampedReference<String> atomicStampedReference = new AtomicStampedReference<>("hello", 0);
        System.out.println("before update: " + atomicStampedReference.getReference() + ", stamp: " + atomicStampedReference.getStamp());
        atomicStampedReference.compareAndSet("hello", "world", 0, 1);
        System.out.println("after update: " + atomicStampedReference.getReference() + ", stamp: " + atomicStampedReference.getStamp());
    }
}
